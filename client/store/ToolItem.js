var _getNewPrice = function(price, toolCount){
  var ret = (!toolCount) ? price : price * (Math.pow(1.5, toolCount));
  return ret;
}
var _getToolCounts = function(toolId, gameId){
  return ToolsBought.find({toolId:toolId,gameId:gameId}).count();
}
updateRate = function(){
  var gameId = Session.get("gameId");
  var rate = _.reduce(ToolsBought.find({gameId:gameId}).fetch(), function(memo, doc){ return memo + (doc.rate * doc.multiplier); }, 0);
  var productionMultiplier = _.reduce(UpgradesBought.find({toolId:null,gameId:gameId}).fetch(), function(memo, doc){ return memo + (doc.multiplier); }, 0);
  productionMultiplier = (productionMultiplier === 0) ? 1 : productionMultiplier;

  var totalRate = rate * productionMultiplier;
  Session.setPersistent('rate', totalRate);
}
Template.ToolItem.events({
   'click .toolItem': function(event) {
        var gameId = Session.get("gameId");

        var toolCount = _getToolCounts(this._id, gameId);


        if(Session.get('counter') < _getNewPrice(this.price, toolCount)){
          console.log("not enough cookies on bank");
          return false;
        }

        multiplier = _.reduce(UpgradesBought.find({toolId:this._id,gameId:gameId}).fetch(), function(memo, doc){ return memo * doc.multiplier; }, 1);;
        ToolsBought.insert({toolId:this._id, rate:this.rate, gameId:gameId, multiplier:multiplier});

        var counter = Session.get('counter');
        Session.setPersistent('counter', (counter-_getNewPrice(this.price, toolCount)));


        updateRate();
   }

});

Template.ToolItem.helpers({
   isNotAvailable: function() {
      return (Session.get('counter') < _getNewPrice(this.price, _getToolCounts(this._id, Session.get('gameId'))));
   },
   toolCount: function(){
     return _getToolCounts(this._id, Session.get('gameId'));
   },
   newPrice: function(){
     return parseInt(_getNewPrice(this.price, _getToolCounts(this._id, Session.get('gameId'))))
   }

});
