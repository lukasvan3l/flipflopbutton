var _getNewPrice = function(price, UpgradeCount){
  //var ret = (!UpgradeCount) ? price : price * (Math.pow(2, UpgradeCount));
  return price;
}
var _getUpgradeCounts = function(UpgradeId, gameId){
  return UpgradesBought.find({upgradeId:UpgradeId,gameId:gameId}).count();
}
Template.UpgradeItem.events({
   'click .upgradeItem': function(event) {
        var gameId = Session.get("gameId");

        var UpgradeCount = _getUpgradeCounts(this._id, gameId);


        if(Session.get('counter') < _getNewPrice(this.price, UpgradeCount)){
          console.log("not enough cookies on bank");
          return false;
        }

        UpgradesBought.insert({upgradeId:this._id, gameId:gameId,multiplier:this.multiplier,toolId:this.toolId});

        var counter = Session.get('counter');
        Session.setPersistent('counter', (counter-_getNewPrice(this.price, UpgradeCount)));

        //updateToolsBought
        Meteor.call("updateToolsBought", gameId, this._id, function (error) {
          // identify the error
          console.log(error);
          // if(typeof error.reason !== "undefined")
          //   alert(error.reason);
            updateRate();
        });
        //updateRate();

   }

});

Template.UpgradeItem.helpers({
   isNotAvailable: function() {
      return (Session.get('counter') < _getNewPrice(this.price, _getUpgradeCounts(this._id, Session.get('gameId'))));
   },
   upgradeCount: function(){
     return _getUpgradeCounts(this._id, Session.get('gameId'));
   },
   newPrice: function(){
     return _getNewPrice(this.price, _getUpgradeCounts(this._id, Session.get('gameId')))
   },
   toolName:function(toolId){
     if(toolId === null){
       return "Cookies"
     }
     var tool = Tools.findOne(toolId);
     return tool.name;
   },
   isVisible: function(){

     var gameId = Session.get("gameId");
     var res = UpgradesBought.find({upgradeId:this._id,gameId:gameId}).count();

     if(res >= this.maxItems){
       return false;
     }
     return true;
   }

});
