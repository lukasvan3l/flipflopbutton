Template.store.helpers({
   tools: function() {
      return Tools.find();
   },
   upgrades: function() {
      return Upgrades.find();
   }
});
