Template.cookie.events({
   'click .js-cookie': function() {
         Session.setPersistent('counter', Session.get('counter')+1);
   }

});

Template.cookie.helpers({
   'currentScore': function() {
      return parseInt(Session.get('counter'));
   }
});

Template.cookie.onRendered(function() {

   // increment number of cookies with rate every second
   Meteor.setInterval(function () {
      var newScore = Session.get('counter') + Session.get('rate');
      Session.setPersistent('counter', newScore);
   }, 1000);

   // store this game current score and rate in DB every 2 seconds
   Meteor.setInterval(function() {
      Games.update(Session.get("gameId"), {
         currentScore: Session.get('counter'),
         currentRate: Session.get('rate')
      });
   },
   2000);

});
