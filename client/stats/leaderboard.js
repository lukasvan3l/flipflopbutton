Template.leaderboard.helpers({
   'games': function() {
      var leaders = Games.find({}, {
         sort: {'currentScore': -1},
         limit: 5
      });
      return leaders;
   },
   'parseInt': function(int) { return parseInt(int); }
});
