Tools = new Mongo.Collection("tools");
ToolsBought = new Mongo.Collection("toolsBought");
Games = new Mongo.Collection("games");

Upgrades = new Mongo.Collection("upgrades");
/*
  {
  _id,
  name,
  price,
  toolId (can be null)
  multiplier
  maxItems (default 1)
}
*/
UpgradesBought = new Mongo.Collection("upgradesBought");
/*
  {
  _id,
  upgradeId,
  gameId,
  multiplier
}
*/
