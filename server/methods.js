Meteor.methods({
  updateToolsBought: function (gameId, upgradeId) {

    check(gameId, String);
    //check(toolId, String);
    check(upgradeId, String);

    var upgrade = Upgrades.findOne(upgradeId);

    var res = UpgradesBought.find({upgradeId:upgrade._id,gameId:gameId}).count();
    if(res > upgrade.maxItems){
      throw new Meteor.Error("maxItems reached", "You already bought the max items possible");
    }
    return ToolsBought.update({toolId:upgrade.toolId,gameId:gameId},{$set:{multiplier:upgrade.multiplier}},{multi:true});
  }
});
