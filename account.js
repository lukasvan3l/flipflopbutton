if (Meteor.isClient) {
  // counter starts at 0
  Session.setPersistent('counter', Session.get('counter') || 0);
  Session.setPersistent('rate', Session.get('rate') || 0);

  var gameId = Session.get("gameId") || Games.insert({});
  console.log(gameId);
  Session.setPersistent('gameId', gameId);

}

if (Meteor.isServer) {

  // ToolsBought.remove({});
  // Tools.remove({});

  Meteor.startup(function () {

    //ToolsBought.remove({});

    if (Upgrades.find().count() === 0){


    if (Tools.find().count() === 0) {

      Tools.insert(
          {
            name:"Cursor",
            rate: 0.1,
            price: 1,
            maxItems: 10
          }
      );
      Tools.insert(
          {
            name:"GrandMa",
            rate: 2,
            price: 10,
            maxItems: 10
          }
      );
      Tools.insert(
          {
            name:"Farm",
            rate: 8,
            price: 100,
            maxItems: 10
          }
      );
      Tools.insert(
          {
            name:"Mine",
            rate: 47,
            price: 1000,
            maxItems: 10
          }
      );}


      Upgrades.insert(
          {
            name:"Platic Mouse",
            multiplier: 1.1,
            price: 1000,
            maxItems: 1,
            toolId:null,
            img: 'img/platicMouse.png'
          }
      );

      var mine = Tools.findOne({name:"Mine"});
      Upgrades.insert(
          {
            name:"Ultimadrill",
            multiplier: 2,
            price: 2000,
            maxItems: 1,
            toolId:mine._id,
            img: 'img/ultimaDrill.png'
          }
      );
      Upgrades.insert(
          {
            name:"Super Ultimadrill",
            multiplier: 3,
            price: 5000,
            maxItems: 1,
            toolId:mine._id,
            img: 'img/superUltimaDrill.png'
          }
      );
    }

  });


}
